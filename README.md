# Functions Available in Maria Commons

```java
// STATEMENTS
DragonCommons.sqlt(name) // Returns a SQLTemplate of the registered SQLT file

// CONNECTIONS
DragonCommons.connection(key) // Borrows a connection with the given name from the pool
DragonCommons.readConnection(operation) // Short-hand for borrowing the default read connection
DragonCommons.writeConnection(operation) // Short-hand for borrowing the default write connection
DragonCommons.ownerConnection(operation) // Short-hand for borrowing the default owner connection

// MONITORABLES
DragonCommons.monitor(monitorable) // Registers the monitorable
DragonCommons.unmonitor(monitorable) // Unregisters the monitorable

// EXECUTORS
DragonCommons.invokeLater(items) // Runs an action on the event thread
DragonCommons.invokeLater(items) // Runs a function on the event thread
DragonCommons.invokeLater(items, delay, time_unit) // Runs an action on the scheduled thread after a given delay
DragonCommons.invokeLater(items, delay, time_unit) // Runs a function on the scheduled thread after a given delay
```
