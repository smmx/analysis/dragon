package @group@;

public class @main@Build {

    public static final String VERSION = "@version@";
    public static final String BUILD_TIMESTAMP = "@build_timestamp@";

}
