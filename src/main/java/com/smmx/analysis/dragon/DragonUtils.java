/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon;

import com.smmx.analysis.dragon.core.database.configuration.DatabaseConfiguration;
import com.smmx.analysis.dragon.core.database.connections.ConnectionPool;
import com.smmx.analysis.dragon.core.database.sql.SQLTemplateManager;
import com.smmx.analysis.dragon.core.executors.EventExecutor;
import com.smmx.analysis.dragon.core.executors.ScheduledExecutor;
import com.smmx.analysis.dragon.core.metrics.Monitor;
import com.smmx.analysis.dragon.core.metrics.Monitorable;
import com.smmx.analysis.dragon.core.pools.Loan;
import com.smmx.maria.commons.database.connections.ConnectionShield;
import com.smmx.maria.commons.database.sql.SQLTemplate;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * @author omiguelc
 */
public class DragonUtils {

    // STATEMENTS
    public static SQLTemplate sqlt(String name) {
        return SQLTemplateManager.getInstance()
            .getTemplate(name);
    }

    // CONNECTIONS
    public static Loan<ConnectionShield> connection(String key) {
        return ConnectionPool.getInstance().loan(key);
    }

    public static Loan<ConnectionShield> readConnection() {
        return connection(DatabaseConfiguration.getInstance().getReadConnection());
    }

    public static Loan<ConnectionShield> writeConnection() {
        return connection(DatabaseConfiguration.getInstance().getWriteConnection());
    }

    public static Loan<ConnectionShield> ownerConnection() {
        return connection(DatabaseConfiguration.getInstance().getOwnerConnection());
    }

    // MONITORABLES
    public static void monitor(Monitorable monitorable) {
        Monitor.getInstance().register(monitorable);
    }

    public static void unmonitor(Monitorable monitorable) {
        Monitor.getInstance().remove(monitorable);
    }

    // EXECUTORS
    public static Future<?> invokeLater(Runnable command) {
        return EventExecutor.getInstance().submit(command);
    }

    public static <T> Future<T> invokeLater(Callable<T> task) {
        return EventExecutor.getInstance().submit(task);
    }

    public static ScheduledFuture<?> invokeLater(Runnable task, long delay, TimeUnit tu) {
        return ScheduledExecutor.getInstance().schedule(task, delay, tu);
    }

    public static <T> ScheduledFuture<T> invokeLater(Callable<T> task, long delay, TimeUnit tu) {
        return ScheduledExecutor.getInstance().schedule(task, delay, tu);
    }

}
