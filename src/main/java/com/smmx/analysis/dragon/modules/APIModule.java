/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.modules;

import com.smmx.analysis.dragon.core.http.API;
import com.smmx.analysis.dragon.endpoints.monitor.MonitorHandler;
import com.smmx.maria.boot.app.AppModule;

/**
 * @author omiguelc
 */
public class APIModule extends AppModule {

    public APIModule() {
        super("API");
    }

    @Override
    public void configure() {
        // DO NOTHING
    }

    @Override
    public void setup() {
        API.getInstance().get("monitor", new MonitorHandler());
    }

}
