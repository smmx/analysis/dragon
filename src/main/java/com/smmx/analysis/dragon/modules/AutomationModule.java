/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.modules;

import com.smmx.analysis.dragon.core.automats.AutomationChainConfiguration;
import com.smmx.analysis.dragon.core.automats.AutomationChainJob;
import com.smmx.analysis.dragon.core.automats.AutomationChainMode;
import com.smmx.analysis.dragon.core.crontab.Crontab;
import com.smmx.maria.boot.app.AppModule;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Trigger;
import org.quartz.TriggerKey;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * @author omiguelc
 */
public class AutomationModule extends AppModule {

    public AutomationModule() {
        super("Automation");
    }

    @Override
    public void configure() {
        // DO NOTHING
    }

    @Override
    public void setup() {
        AutomationChainMode mode = AutomationChainConfiguration.getInstance().getMode();

        if (mode.equals(AutomationChainMode.CRONJOB)) {
            // DEFINE JOB
            JobDetail job = newJob(AutomationChainJob.class)
                .withIdentity(new JobKey("Automation Chain", "main"))
                .build();

            // Trigger the job to run now, and then repeat
            Trigger trigger = newTrigger()
                .withIdentity(new TriggerKey("Automation Chain", "main"))
                .startNow()
                .withSchedule(cronSchedule(
                    AutomationChainConfiguration.getInstance().getSchedule()
                ))
                .build();

            // Tell quartz to schedule the job using our trigger
            Crontab.getInstance().scheduleJob(job, trigger);
        }

    }

}
