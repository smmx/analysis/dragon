/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.modules;

import com.smmx.analysis.dragon.core.database.configuration.DatabaseConfiguration;
import com.smmx.analysis.dragon.core.database.sql.SQLTemplateFile;
import com.smmx.analysis.dragon.core.database.sql.SQLTemplateManager;
import com.smmx.analysis.dragon.core.metrics.Monitor;
import com.smmx.analysis.dragon.monitorables.SystemMonitorable;
import com.smmx.maria.boot.MariaBootException;
import com.smmx.maria.boot.app.AppModule;
import com.smmx.maria.commons.database.defaults.DefaultSQLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.sql;

/**
 * @author omiguelc
 */
public class CoreModule extends AppModule {

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(CoreModule.class);

    // MEMBER
    public CoreModule() {
        super("Dragon Core");
    }

    @Override
    public void configure() {
        System.setProperty("java.awt.headless", "true");

        // LOG
        LOGGER.info("Using timezone: {}.", System.getProperty("user.timezone"));

        // SET ORACLE PROPERTIES
        System.setProperty("oracle.net.tns_admin", DatabaseConfiguration.getInstance().getTNSAdmin());
        System.setProperty("oracle.jdbc.J2EE13Compliant", "true");

        // LOG
        LOGGER.info("Using tnsadmin: {}.", System.getProperty("oracle.net.tns_admin"));

        // GET RESOURCE PATH
        Path dragon = null;

        try {
            URL dragon_url = ClassLoader.getSystemResource("dragon");

            if (dragon_url != null) {
                URI dragon_uri = dragon_url.toURI();

                if (dragon_uri.getScheme().equals("jar")) {
                    Map<String, String> env = new HashMap<>();
                    env.put("create", "true");

                    dragon = FileSystems.newFileSystem(
                        dragon_uri, env
                    ).getPath("/");
                } else {
                    dragon = Paths.get(
                        dragon_uri
                    );
                }
            }
        } catch (URISyntaxException ex) {
            throw new MariaBootException("Failed to get resource directory.", ex);
        } catch (IOException ex) {
            throw new MariaBootException("Failed to load zip.", ex);
        }

        // SQL DEFINITIONS
        DatabaseConfiguration.getInstance().getSQLDefinitions()
            .forEach((name, value) -> {
                DefaultSQLContext.getInstance().define(name, sql(value));
            });

        // RESOURCE RELATED STUFF
        if (dragon == null) {
            return;
        }

        // SQL TEMPLATES
        try {
            Files.walk(dragon)
                .forEach(path -> {
                    // SKIP ROOT
                    if (path.getFileName() == null) {
                        return;
                    }

                    // CHECK FILENAME
                    String filename = path.getFileName().toString();

                    if (!filename.endsWith(".sql")) {
                        return;
                    }

                    LOGGER.info("Loading sql template {}", path.getFileName());

                    // LOAD
                    InputStream is;

                    try {
                        is = Files.newInputStream(path);
                    } catch (IOException ex) {
                        throw new MariaBootException("Failed to load sql template.", ex);
                    }

                    SQLTemplateFile sqlt;

                    try {
                        sqlt = new SQLTemplateFile(is);
                    } catch (IOException ex) {
                        throw new MariaBootException("Failed to load sql template.", ex);
                    }

                    SQLTemplateManager.getInstance()
                        .registerTemplate(
                            sqlt.getName(),
                            () -> {
                                return sql(sqlt.getSQL())
                                    .registerFields(sqlt.getFields())
                                    .registerParameters(sqlt.getParameters());
                            }
                        );
                });
        } catch (IOException ex) {
            throw new MariaBootException("Failed to walk dragon.", ex);
        }
    }

    @Override
    public void setup() {
        // MONITORABLES
        Monitor.getInstance().register(new SystemMonitorable());
    }

}
