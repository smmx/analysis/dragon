/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.http.configuration;

import com.smmx.maria.boot.configuration.Configuration;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.smmx.maria.commons.MariaCommons.dp;
import static com.smmx.maria.commons.MariaCommons.vp;

/**
 * @author Osvaldo Miguel Colin
 */
public class HttpConfiguration {

    // SINGLETON
    private static HttpConfiguration INSTANCE;

    static {
        INSTANCE = null;
    }

    public static HttpConfiguration getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new HttpConfiguration();
        }

        return INSTANCE;
    }

    // MEMBER
    private final String host;
    private final int port;
    private final String base_path;

    private final Set<BindConfiguration> binds;

    private HttpConfiguration() {
        Map<String, Object> api_section = dp()
            .require("http")
            .asDictionary()
            .notNull()
            .apply(Configuration.getInstance());

        this.host = dp()
            .require("host")
            .asString()
            .notNull()
            .apply(api_section);

        this.port = dp()
            .require("port")
            .asInteger()
            .notNull()
            .apply(api_section);

        this.base_path = dp()
            .require("base_path")
            .asString()
            .notNull()
            .apply(api_section);

        this.binds = dp()
            .get("binds", Collections.emptyList())
            .asList()
            .notNull()
            .apply(api_section)
            .stream()
            .map(vp()
                .asDictionary()
                .notNull()
                .map(BindConfiguration::new)
            )
            .collect(Collectors.toSet());
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getBasePath() {
        return base_path;
    }

    public Set<BindConfiguration> getBinds() {
        return binds;
    }

    // CLASSES
    public class BindConfiguration {

        // MEMBER
        private final String host;
        private final Integer port;

        public BindConfiguration(Map<String, Object> bind) {
            this.host = dp()
                .require("host")
                .asString()
                .notNull()
                .apply(bind);

            this.port = dp()
                .get("port", HttpConfiguration.this.port)
                .asInteger()
                .notNull()
                .apply(bind);
        }

        public BindConfiguration(String host) {
            this(host, HttpConfiguration.this.port);
        }

        public BindConfiguration(String host, Integer port) {
            this.host = host;
            this.port = port;
        }

        public String getHost() {
            return host;
        }

        public Integer getPort() {
            return port;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 83 * hash + Objects.hashCode(this.host);
            hash = 83 * hash + Objects.hashCode(this.port);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }

            if (obj == null) {
                return false;
            }

            if (getClass() != obj.getClass()) {
                return false;
            }

            final BindConfiguration other = (BindConfiguration) obj;

            if (!Objects.equals(this.host, other.host)) {
                return false;
            }

            return Objects.equals(this.port, other.port);
        }

    }

}
