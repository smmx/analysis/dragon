package com.smmx.analysis.dragon.core.http;

import com.smmx.maria.commons.api.APIResponse;
import io.javalin.http.Context;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import static com.smmx.maria.commons.MariaCommons.apiSuccess;

public class APIUtils {

    public static Map<String, Object> bodyParamMap(Context ctx) {
        // CHECK
        String content_type = ctx.header("Content-Type");

        if (!Objects.equals(content_type, "application/json")) {
            return null;
        }

        // PARSE
        String body = ctx.body();

        if (body.isEmpty()) {
            return Collections.emptyMap();
        } else {
            return new JSONObject(body).toMap();
        }
    }

    public static Map<String, Object> multiformParamMap(Context ctx) {
        String request = ctx.formParam("request");

        // CHECK
        if (request == null || request.isEmpty()) {
            return Collections.emptyMap();
        }

        // PARSE
        return new JSONObject(request).toMap();
    }

    public static void writeDictionary(Context ctx, Map<String, Object> payload) {
        writeAPIResponse(
            ctx, apiSuccess(payload)
        );
    }

    public static void writeAPIResponse(Context ctx, APIResponse response) {
        ctx.status(response.status());
        ctx.result(response.toString());
    }

}
