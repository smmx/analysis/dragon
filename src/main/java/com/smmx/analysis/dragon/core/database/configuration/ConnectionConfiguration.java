/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.database.configuration;

import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author omiguelc
 */
public class ConnectionConfiguration {

    private final String name;
    private final String username;
    private final String password;
    private final String url;

    public ConnectionConfiguration(Map<String, Object> connection_info) {
        this.name = dp()
            .require("name")
            .asString()
            .notNull()
            .apply(connection_info);

        this.username = dp()
            .require("username")
            .asString()
            .notNull()
            .apply(connection_info);

        this.password = dp()
            .require("password")
            .asString()
            .notNull()
            .apply(connection_info);

        this.url = dp()
            .require("url")
            .asString()
            .notNull()
            .apply(connection_info);
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getURL() {
        return url;
    }

}
