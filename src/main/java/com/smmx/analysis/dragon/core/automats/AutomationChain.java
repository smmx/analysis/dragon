/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.automats;

import com.smmx.analysis.dragon.core.metrics.Metrics;
import com.smmx.analysis.dragon.core.metrics.Monitor;
import com.smmx.analysis.dragon.core.metrics.Monitorable;
import com.smmx.analysis.dragon.core.metrics.utils.ProgressMeter;
import com.smmx.analysis.dragon.core.multithreading.MultithreadingConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author omiguelc
 */
public class AutomationChain implements Monitorable {

    // SINGLETON
    private static AutomationChain INSTANCE;

    static {
        INSTANCE = null;
    }

    public static AutomationChain getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AutomationChain();
        }

        return INSTANCE;
    }

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(AutomationChain.class);

    // MEMBER
    private final ExecutorService executor;
    private final List<Automat> automations;

    private final Object mutex;

    private final ProgressMeter chain_meter;
    private final ProgressMeter automat_meter;

    private String current_automat;
    private boolean automat_running;
    private int successful_runs;
    private int blocked_runs;
    private int failed_runs;

    private final ReentrantLock lock;

    public AutomationChain() {
        this.executor = Executors.newWorkStealingPool(
            MultithreadingConfiguration.getInstance().getThreads()
        );

        this.automations = new ArrayList<>();

        this.mutex = new Object();

        this.chain_meter = new ProgressMeter();
        this.automat_meter = new ProgressMeter();

        this.current_automat = null;
        this.automat_running = false;
        this.successful_runs = 0;
        this.blocked_runs = 0;
        this.failed_runs = 0;

        this.lock = new ReentrantLock();

        // REGISTER MONITORABLE
        Monitor.getInstance().register(this);
    }

    public AutomationChain register(Automat automat) {
        automations.add(automat);
        return this;
    }

    public void run() {
        // SAFETY CHECK
        if (!lock.tryLock()) {
            // INCREASE BLOCKED RUNS
            synchronized (mutex) {
                blocked_runs++;
            }

            // THROW
            throw new AutomationChainRunException("Can't run this simultaneously; another thread is currently running this method.");
        }

        try {
            // METER
            synchronized (mutex) {
                chain_meter.init(automations.size());
            }

            // RUN
            for (Automat automat : automations) {
                synchronized (mutex) {
                    current_automat = automat.getName();
                }

                // SETUP
                automat.setup();

                // GET TASKS
                List<AutomatTask> tasks = automat.getTasks();

                // METER
                synchronized (mutex) {
                    automat_meter.init(tasks.size());
                    automat_running = true;
                }

                // LATCH
                CountDownLatch latch = new CountDownLatch(
                    tasks.size()
                );

                // SUBMIT
                tasks.forEach(task -> {
                    executor.submit(() -> {
                        try {
                            task.run();
                        } catch (Throwable t) {
                            LOGGER.error("Unhandled exception.", t);
                        } finally {
                            // METER
                            synchronized (mutex) {
                                automat_meter.increaseProgress();
                            }

                            // LATCH
                            latch.countDown();
                        }
                    });
                });

                // CLEAN UP
                automat.cleanUp();

                // AWAIT
                try {
                    latch.await();
                } catch (InterruptedException ex) {
                    break;
                }

                // METER
                synchronized (mutex) {
                    chain_meter.increaseProgress();
                    current_automat = null;
                    automat_running = false;
                }
            }

            // INCREASE SUCCESSFUL RUN
            synchronized (mutex) {
                successful_runs++;
            }
        } catch (Throwable thrwbl) {
            // INCREASE FAILED RUN
            synchronized (mutex) {
                failed_runs++;
            }

            // FORWARD
            throw thrwbl;
        } finally {
            // UNLOCK
            lock.unlock();
        }
    }

    @Override
    public void collectMetrics(Metrics metrics) {
        synchronized (mutex) {
            metrics.getDictionary("automation")
                .getDictionary("chain")
                .getDictionary("runs")
                .put("successful", successful_runs)
                .put("failed", failed_runs)
                .put("blocked", blocked_runs);

            metrics.getDictionary("automation")
                .getDictionary("chain")
                .put("progress", chain_meter.getPercentage())
                .put("n", chain_meter.getLastMeasurementProgress())
                .put("total_n", chain_meter.getGoal())
                .put("uptime", chain_meter.getElapsedTime())
                .put("eta", chain_meter.getEstimatedTimeToFinish());

            if (current_automat != null) {
                metrics.getDictionary("automation")
                    .getDictionary("automat")
                    .put("name", current_automat);
            }

            if (automat_running) {
                metrics.getDictionary("automation")
                    .getDictionary("automat")
                    .put("progress", automat_meter.getPercentage())
                    .put("n", automat_meter.getLastMeasurementProgress())
                    .put("total_n", automat_meter.getGoal())
                    .put("uptime", automat_meter.getElapsedTime())
                    .put("eta", automat_meter.getEstimatedTimeToFinish())
                    .put("speed", automat_meter.getPartialSpeed());
            }
        }
    }

}
