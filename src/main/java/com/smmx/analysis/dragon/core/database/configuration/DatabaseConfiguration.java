/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.database.configuration;

import com.smmx.maria.boot.app.AppRuntime;
import com.smmx.maria.boot.configuration.Configuration;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;
import static com.smmx.maria.commons.MariaCommons.vp;

/**
 * @author Osvaldo Miguel Colin
 */
public class DatabaseConfiguration {

    // SINGLETON
    private static DatabaseConfiguration INSTANCE;

    static {
        INSTANCE = null;
    }

    public static DatabaseConfiguration getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DatabaseConfiguration();
        }

        return INSTANCE;
    }

    // MEMBER
    private final String read_connection;
    private final String write_connection;
    private final String owner_connection;

    private final String tnsadmin;
    private final Map<String, ConnectionConfiguration> connections;

    private final Map sql_definitions;

    private DatabaseConfiguration() {
        Map<String, Object> database_section = dp()
            .require("database")
            .asDictionary()
            .notNull()
            .apply(Configuration.getInstance());

        this.read_connection = dp()
            .get("read", "read")
            .asString()
            .notNull()
            .apply(database_section);

        this.write_connection = dp()
            .get("write", "write")
            .asString()
            .notNull()
            .apply(database_section);

        this.owner_connection = dp()
            .get("owner", "owner")
            .asString()
            .notNull()
            .apply(database_section);

        //////////////////////////TNS ADMIN/////////////////////////////////////
        this.tnsadmin = dp()
            .get("tnsadmin")
            .asString()
            .ifNull("tnsadmin")
            .map(value -> {
                if (!value.startsWith("/")) {
                    value = AppRuntime.home().resolve(value).toString();
                }

                return value;
            })
            .apply(database_section);

        //////////////////////////CONNECTIONS///////////////////////////////////
        this.connections = new HashMap<>();

        // GET
        List conf_connections = dp()
            .require("connections")
            .asList()
            .notNull()
            .apply(database_section);

        // FOR EACH
        conf_connections.forEach((conf_connection_obj) -> {
            // GET
            Map<String, Object> conf_connection = vp()
                .asDictionary()
                .notNull()
                .apply(conf_connection_obj);

            // PARSE
            ConnectionConfiguration connection = new ConnectionConfiguration(conf_connection);

            // ADD
            connections.put(connection.getName(), connection);
        });

        ///////////////////////TABLE ALIASES////////////////////////////////////
        this.sql_definitions = new HashMap<>();

        // GET
        Map<String, Object> conf_aliases = dp()
            .get("sql-definitions", Collections.emptyMap())
            .asDictionary()
            .notNull()
            .apply(database_section);

        // FOR EACH
        conf_aliases.forEach((alias, value) -> {
            alias = vp()
                .asString()
                .notNull()
                .apply(alias);

            value = vp()
                .asString()
                .notNull()
                .apply(value);

            this.sql_definitions.put(alias, value);
        });
    }

    public String getReadConnection() {
        return read_connection;
    }

    public String getWriteConnection() {
        return write_connection;
    }

    public String getOwnerConnection() {
        return owner_connection;
    }

    public String getTNSAdmin() {
        return tnsadmin;
    }

    public Map<String, ConnectionConfiguration> getConnections() {
        return connections;
    }

    public Map<String, String> getSQLDefinitions() {
        return sql_definitions;
    }
}
