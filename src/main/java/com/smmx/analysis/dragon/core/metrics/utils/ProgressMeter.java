/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.metrics.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayDeque;

/**
 * @author omiguelc
 */
public class ProgressMeter {

    // MEMBER
    private int goal;
    private int stat_window;
    private ArrayDeque<Measurement> measurements;
    private Chronometer chronometer;

    // OPERATION
    public ProgressMeter() {
        init(1);
    }

    public synchronized void init(int goal) {
        init(goal, 100);
    }

    public synchronized void init(int goal, int stat_window) {
        if (goal == 0) {
            goal = 1;
        }

        this.goal = goal;
        this.stat_window = stat_window;
        this.measurements = new ArrayDeque();
        this.chronometer = new Chronometer();

        // START
        chronometer.start();
        setProgress(0);
    }

    public synchronized void increaseProgress() {
        increaseProgress(1);
    }

    public synchronized void increaseProgress(int n) {
        setProgress(getLastMeasurementProgress() + n);
    }

    public synchronized void setProgress(int progress) {
        measurements.add(new Measurement(progress));

        while (measurements.size() > stat_window) {
            measurements.pop();
        }

        if (progress == goal) {
            chronometer.stop();
        }
    }

    // PROGRESS
    public synchronized int getGoal() {
        return goal;
    }

    public synchronized double getPercentage() {
        double progress = getLastMeasurementProgress();
        double percentage = (progress / ((double) goal)) * 100d;
        BigDecimal big_decimal = new BigDecimal(percentage);
        big_decimal.setScale(2, RoundingMode.HALF_UP);
        return big_decimal.doubleValue();
    }

    public synchronized int getPartialProgress() {
        return getLastMeasurementProgress() - getFirstMeasurementProgress();
    }

    public synchronized int getPendingProgress() {
        return goal - getLastMeasurementProgress();
    }

    public synchronized int getFirstMeasurementProgress() {
        if (measurements.isEmpty()) {
            return -1;
        }

        return measurements.getFirst().getProgress();
    }

    public synchronized int getLastMeasurementProgress() {
        if (measurements.isEmpty()) {
            return -1;
        }

        return measurements.getLast().getProgress();
    }

    // TIME
    public synchronized double getStartTime() {
        return ((double) chronometer.getStartTime()) / 1000d;
    }

    public synchronized double getStopTime() {
        return ((double) chronometer.getStopTime()) / 1000d;
    }

    public synchronized double getElapsedTime() {
        return ((double) chronometer.getElapsedTime()) / 1000d;
    }

    public synchronized double getPartialElapsedTime() {
        double endtime = getLastMeasurementTime();
        double starttime = getFirstMeasurementTime();
        return endtime - starttime;
    }

    public synchronized double getEstimatedTimeToFinish() {
        double pending_progress = getPendingProgress();
        double speed = getPartialSpeed();

        if (speed == 0) {
            return -1;
        }

        return pending_progress / speed;
    }

    public synchronized double getFirstMeasurementTime() {
        if (measurements.isEmpty()) {
            return -1;
        }

        return measurements.getFirst().getTime();
    }

    public synchronized double getLastMeasurementTime() {
        if (measurements.isEmpty()) {
            return -1;
        }

        return measurements.getLast().getTime();
    }

    // SPEED
    public synchronized double getOverallSpeed() {
        return getLastMeasurementProgress() / getElapsedTime();
    }

    public synchronized double getPartialSpeed() {
        double partial_elapsed_time = getPartialElapsedTime();
        double partial_progress = getPartialProgress();

        if (partial_elapsed_time == 0) {
            return -1;
        }

        return partial_progress / partial_elapsed_time;
    }

    // MEMBERS
    private class Measurement {

        private final int progress;
        private final double time;

        public Measurement(int progress) {
            this.progress = progress;
            this.time = ((double) chronometer.getCurrentTime()) / 1000d;
        }

        public int getProgress() {
            return progress;
        }

        public double getTime() {
            return time;
        }
    }
}
