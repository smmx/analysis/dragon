/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.automats;

import com.smmx.maria.boot.configuration.Configuration;

import java.util.Collections;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class AutomationChainConfiguration {

    // SINGLETON
    private static AutomationChainConfiguration INSTANCE;

    static {
        INSTANCE = null;
    }

    public static AutomationChainConfiguration getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AutomationChainConfiguration();
        }

        return INSTANCE;
    }

    // MEMBER
    private final AutomationChainMode mode;
    private final String schedule;

    private AutomationChainConfiguration() {
        Map<String, Object> automation_section = dp()
            .get("automation", Collections.emptyMap())
            .asDictionary()
            .notNull()
            .apply(Configuration.getInstance());

        this.mode = dp()
            .get("mode")
            .asEnum(AutomationChainMode.class)
            .ifNull(AutomationChainMode.CRONJOB)
            .apply(automation_section);

        this.schedule = dp()
            .require("schedule")
            .asString()
            .notNull()
            .apply(automation_section);
    }

    public AutomationChainMode getMode() {
        return mode;
    }

    public String getSchedule() {
        return schedule;
    }

}
