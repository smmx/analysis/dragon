/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.metrics.utils;

/**
 * @author omiguelc
 */
public class Chronometer {

    // MEMBER
    private long start_time;
    private long stop_time;
    private boolean is_running;

    public Chronometer() {
        this.is_running = false;
    }

    public void start() {
        is_running = true;
        start_time = System.currentTimeMillis();
    }

    public void stop() {
        is_running = false;
        stop_time = System.currentTimeMillis();
    }

    public boolean isRunning() {
        return is_running;
    }

    public long getStartTime() {
        return start_time;
    }

    public long getStopTime() {
        return stop_time;
    }

    public long getCurrentTime() {
        if (is_running) {
            return System.currentTimeMillis();
        }

        // RETURN
        return stop_time;
    }

    public long getElapsedTime() {
        return getCurrentTime() - getStartTime();
    }
}
