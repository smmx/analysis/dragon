/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.automats;

import com.smmx.analysis.dragon.core.metrics.Metrics;
import com.smmx.analysis.dragon.core.metrics.Monitor;
import com.smmx.analysis.dragon.core.metrics.Monitorable;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * @author omiguelc
 */
public class AutomationChainRunner implements Monitorable {

    // SINGLETON
    private static AutomationChainRunner INSTANCE;

    static {
        INSTANCE = null;
    }

    public static AutomationChainRunner getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AutomationChainRunner();
        }

        return INSTANCE;
    }

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(AutomationChainRunner.class);

    // MEMBER
    private final Object mutex;

    private Scheduler scheduler;

    private AutomationChainRunnerState state;
    private int late_count;
    private boolean rerun;

    private AutomationChainRunner() {
        mutex = new Object();

        scheduler = null;

        state = AutomationChainRunnerState.STANDBY;
        late_count = 0;
        rerun = false;

        // REGISTER MONITORABLE
        Monitor.getInstance().register(this);
    }

    public void run(JobExecutionContext context) throws JobExecutionException {
        // LOGIC
        boolean start_new_run = false;

        synchronized (mutex) {
            if (AutomationChainRunnerState.STANDBY.equals(state)) {
                // UPDATE STATE
                state = AutomationChainRunnerState.RUNNING;

                // GET SCHEDULER
                Scheduler this_scheduler = context.getScheduler();

                if (scheduler == null) {
                    scheduler = this_scheduler;
                } else if (!Objects.equals(scheduler, this_scheduler)) {
                    throw new IllegalStateException("Another scheduler was used for the automat runner. Only one must use this instance.");
                }

                // SCHEDULE START
                start_new_run = true;
            } else {
                late_count = late_count + 1;

                if (!rerun) {
                    LOGGER.info("Automat chain is behind schedule. Automat chain scheduled to start as soon as the old one is done. Late count: {}.", late_count);

                    rerun = true;
                } else {
                    LOGGER.info("Automat chain is behind schedule. Late count: {}.", late_count);
                }

                // UPDATE STATE
                state = AutomationChainRunnerState.RUNNING_LATE;
            }
        }

        // RUN
        if (start_new_run) {
            // RUN AUTOMAT CHAIN
            AutomationChain.getInstance().run();

            // FINISH
            cleanUp();
        }

    }

    private void cleanUp() {
        boolean start_new_run;

        synchronized (mutex) {
            start_new_run = rerun;

            rerun = false;
            late_count = 0;
        }

        if (start_new_run) {
            synchronized (mutex) {
                try {
                    boolean shutdown = scheduler.isShutdown();
                    boolean standby = scheduler.isInStandbyMode();

                    if (!shutdown && !standby) {
                        // UPDATE STATE
                        state = AutomationChainRunnerState.RUNNING;

                        // RUN ONLY IF THE SERVICE IS STILL GOING
                        start_new_run = true;
                    } else if (shutdown) {
                        LOGGER.info("Late automat chain canceled because the crontab was shutdown.", late_count);

                        start_new_run = false;
                    } else if (standby) {
                        LOGGER.info("Late automat chain canceled because the crontab is in standby mode.", late_count);

                        start_new_run = false;
                    }
                } catch (SchedulerException ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            }
        }

        if (start_new_run) {
            // RUN AUTOMAT CHAIN
            AutomationChain.getInstance().run();

            // FINISH
            cleanUp();
        }

        // UPDATE STATE
        synchronized (mutex) {
            state = AutomationChainRunnerState.STANDBY;
        }
    }

    @Override
    public void collectMetrics(Metrics metrics) {
        synchronized (mutex) {
            metrics.getDictionary("automation")
                .getDictionary("cronjob")
                .put("state", state.name())
                .put("late_count", late_count);
        }
    }
}
