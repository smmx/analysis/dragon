/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.metrics;

/**
 * @author omiguelc
 */
public interface Monitorable {

    void collectMetrics(Metrics metrics);

}
