/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.multithreading;

import com.smmx.maria.boot.configuration.Configuration;

import java.util.Collections;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class MultithreadingConfiguration {

    // SINGLETON
    private static MultithreadingConfiguration INSTANCE;

    static {
        INSTANCE = null;
    }

    public static MultithreadingConfiguration getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MultithreadingConfiguration();
        }

        return INSTANCE;
    }

    // MEMBER
    private final int threads;

    private MultithreadingConfiguration() {
        Map<String, Object> automation_section = dp()
            .get("multithreading", Collections.emptyMap())
            .asDictionary()
            .notNull()
            .apply(Configuration.getInstance());

        this.threads = dp()
            .get("threads", Runtime.getRuntime().availableProcessors())
            .asInteger()
            .notNull()
            .apply(automation_section);
    }

    public int getThreads() {
        return threads;
    }

}
