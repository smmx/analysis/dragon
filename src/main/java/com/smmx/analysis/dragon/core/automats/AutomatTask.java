/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.automats;

import com.smmx.maria.commons.references.ThrowableReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author omiguelc
 */
public abstract class AutomatTask implements Runnable {

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(AutomatTask.class);

    // MEMBER
    @Override
    public final void run() {
        ThrowableReference<Throwable> throwable_ref = new ThrowableReference();

        // SETUP
        setup();

        try {
            // RUN
            doTask();
        } catch (Throwable ex) {
            // CATCH
            throwable_ref.set(ex);
        }

        // CLEANUP
        cleanUp();

        // DONE
        if (throwable_ref.isPresent()) {
            onError(throwable_ref.get());
        } else {
            onCompletion();
        }
    }

    // USER IMPLEMENTATIONS
    protected abstract void setup();

    protected abstract void doTask();

    protected abstract void cleanUp();

    protected void onError(Throwable throwable) {
        LOGGER.error("Unhandled exception.", throwable);
    }

    protected void onCompletion() {
        // DO NOTHING
    }
}
