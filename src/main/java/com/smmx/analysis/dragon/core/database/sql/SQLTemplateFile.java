/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.database.sql;

import org.yaml.snakeyaml.Yaml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author omiguelc
 */
public class SQLTemplateFile {

    // STATIC
    private static final String HEADER_TAG = "@Header";
    private static final String BODY_TAG = "@Body";

    // MEMBER
    private final String name;
    private final Map<String, Class<?>> fields;
    private final Map<String, Class<?>> parameters;
    private final String template;

    public SQLTemplateFile(InputStream in) throws IOException {
        // READER
        InputStreamReader is_reader = new InputStreamReader(in);
        BufferedReader reader = new BufferedReader(is_reader);

        // READ PARTS
        String line;

        StringBuilder header_sb = new StringBuilder();
        boolean header_open = false;
        boolean header_closed = false;

        StringBuilder body_sb = new StringBuilder();
        boolean body_open = false;
        boolean body_closed = false;

        while ((line = reader.readLine()) != null) {
            String trimmed_line = line.trim();

            if (!line.isEmpty()) {
                if (HEADER_TAG.equals(trimmed_line)) {
                    body_closed = body_open;
                    header_open = true;
                } else if (BODY_TAG.equals(trimmed_line)) {
                    header_closed = header_open;
                    body_open = true;
                } else if (header_open && !header_closed) {
                    header_sb.append(line).append(System.lineSeparator());
                } else if (body_open && !body_closed) {
                    body_sb.append(line).append(System.lineSeparator());
                }
            }
        }

        // PARSE HEADER
        String header_str = header_sb.toString().replaceAll("\\t", "    ");

        Map<String, Object> header = new Yaml().loadAs(header_str, Map.class);

        String name = dp()
            .require("name")
            .asString()
            .notNull()
            .apply(header);

        Map<String, Object> fields = dp()
            .get("fields")
            .asDictionary()
            .ifNull(Collections.emptyMap())
            .apply(header);

        Map<String, Object> parameters = dp()
            .get("parameters")
            .asDictionary()
            .ifNull(Collections.emptyMap())
            .apply(header);

        // SET
        this.name = name;
        this.fields = new HashMap<>();
        this.parameters = new HashMap<>();
        this.template = body_sb.toString();

        // POPULATE FIELDS
        fields.forEach((key, dt) -> {
            this.fields.put(
                key,
                SQLTemplateManager.getInstance()
                    .getDatatype((String) dt)
            );
        });

        // POPULATE PARAMETERS
        parameters.forEach((key, dt) -> {
            this.parameters.put(
                key,
                SQLTemplateManager.getInstance()
                    .getDatatype((String) dt)
            );
        });
    }

    public String getName() {
        return name;
    }

    public Map<String, Class<?>> getFields() {
        return fields;
    }

    public Map<String, Class<?>> getParameters() {
        return parameters;
    }

    public String getSQL() {
        return template;
    }

}
