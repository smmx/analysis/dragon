/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.metrics;

import java.util.HashMap;
import java.util.Map;

/**
 * @author omiguelc
 */
public class Metrics {

    private final Map<String, Object> node;

    public Metrics() {
        this.node = new HashMap<>();
    }

    public Metrics(Map<String, Object> node) {
        this.node = node;
    }

    public Map<String, Object> toMap() {
        return node;
    }

    public Metrics getDictionary(String key) {
        Map<String, Object> child_node;

        if (this.node.containsKey(key)) {
            // GET NODE
            child_node = (Map<String, Object>) this.node.get(key);
        } else {
            // CREATE NEW NODE
            child_node = new HashMap<>();

            this.node.put(key, child_node);
        }

        return new Metrics(child_node);
    }

    public Metrics put(String key, Object value) {
        node.put(key, value);
        return this;
    }

}
