/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.automats;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author omiguelc
 */
public class AutomationChainJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        AutomationChainRunner.getInstance().run(context);
    }

}
