/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.database.builders.privileges;

import com.smmx.analysis.dragon.core.database.configuration.DatabaseConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author Osvaldo Miguel Colin
 */
public class PrivilegeGroup {

    private final List<String> users;
    private final List<ObjectPrivileges> object_privileges;

    public PrivilegeGroup() {
        this.users = new ArrayList<>();
        this.object_privileges = new ArrayList<>();
    }

    public PrivilegeGroup addConnection(String connection_name) {
        String user = DatabaseConfiguration.getInstance()
            .getConnections()
            .values()
            .stream()
            .filter(connection -> connection.getName().equals(connection_name))
            .map(connection -> connection.getUsername())
            .findFirst()
            .orElseThrow(NoSuchElementException::new);

        this.users.add(user);

        return this;
    }

    public PrivilegeGroup addObjectPrivileges(String objectname, List<Privilege> privileges) {
        objectname = DatabaseConfiguration.getInstance()
            .getSQLDefinitions()
            .get(objectname);

        this.object_privileges.add(new ObjectPrivileges(
            objectname,
            privileges
        ));

        return this;
    }

    public List<String> getUsers() {
        return users;
    }

    public List<ObjectPrivileges> getObjectPrivileges() {
        return object_privileges;
    }

}
