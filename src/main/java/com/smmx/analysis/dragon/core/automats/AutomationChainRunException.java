/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.automats;

/**
 * @author omiguelc
 */
public class AutomationChainRunException extends RuntimeException {

    public AutomationChainRunException(String msg) {
        super(msg);
    }

}
