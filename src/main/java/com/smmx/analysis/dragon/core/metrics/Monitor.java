/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.metrics;

import com.smmx.analysis.dragon.core.executors.ScheduledExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author omiguelc
 */
public class Monitor {

    // SINGLETON
    private static Monitor INSTANCE;

    static {
        INSTANCE = null;
    }

    public static Monitor getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Monitor();
        }

        return INSTANCE;
    }

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(Monitor.class);

    // MEMBER
    private final List<Monitorable> monitorables;
    private final Map<String, Object> metrics;

    private Monitor() {
        this.monitorables = new ArrayList<>();
        this.metrics = new HashMap<>();

        ScheduledExecutor.getInstance().scheduleAtFixedRate(
            this::collect, 0, 1000, TimeUnit.MILLISECONDS
        );
    }

    public void register(Monitorable monitorable) {
        synchronized (monitorables) {
            monitorables.add(monitorable);
        }
    }

    public void remove(Monitorable monitorable) {
        synchronized (monitorables) {
            monitorables.remove(monitorable);
        }
    }

    public Map<String, Object> getMetrics() {
        Map<String, Object> clone;

        synchronized (metrics) {
            clone = new HashMap<>(metrics);
        }

        return Collections.unmodifiableMap(clone);
    }

    public void collect() {
        Metrics collected_metrics = new Metrics();

        synchronized (monitorables) {
            monitorables.forEach(monitorable -> {
                try {
                    monitorable.collectMetrics(collected_metrics);
                } catch (Exception ex) {
                    LOGGER.warn("A monitorable failed to collect its metrics.", ex);
                }
            });
        }

        synchronized (metrics) {
            this.metrics.clear();
            this.metrics.putAll(
                collected_metrics.toMap()
            );
        }
    }

}
