/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.http;

import com.smmx.analysis.dragon.core.http.configuration.HttpConfiguration;
import io.javalin.Javalin;
import io.javalin.core.security.AccessManager;
import io.javalin.core.security.Role;
import io.javalin.http.ExceptionHandler;
import io.javalin.http.Handler;
import io.javalin.http.sse.SseClient;
import io.javalin.http.staticfiles.Location;
import io.javalin.websocket.WsExceptionHandler;
import io.javalin.websocket.WsHandler;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author Osvaldo Miguel Colin
 */
public class API {

    // SINGLETON
    private static API INSTANCE;

    static {
        INSTANCE = null;
    }

    public static API getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new API();
        }

        return INSTANCE;
    }

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(API.class);

    // MEMBER
    private final Javalin javalin;

    private API() {
        javalin = Javalin.create(config -> {
            config.server(() -> {
                Server server = new Server();

                // GET HOSTS
                Set<HttpConfiguration.BindConfiguration> binds = new HashSet<>(
                    HttpConfiguration.getInstance().getBinds()
                );

                if (binds.isEmpty()) {
                    try {
                        Enumeration<NetworkInterface> nics = NetworkInterface.getNetworkInterfaces();

                        while (nics.hasMoreElements()) {
                            NetworkInterface nic = nics.nextElement();
                            Enumeration<InetAddress> addrs = nic.getInetAddresses();

                            while (addrs.hasMoreElements()) {
                                InetAddress addr = addrs.nextElement();
                                binds.add(HttpConfiguration.getInstance().new BindConfiguration(
                                    addr.getHostAddress()
                                ));
                            }
                        }
                    } catch (SocketException ex) {
                        LOGGER.error("Failed to get network interfaces.", ex);
                    }
                }

                // BUILD CONNECTORS
                Connector[] connectors = new Connector[binds.size()];

                binds
                    .stream()
                    .map((bind) -> {
                        ServerConnector serverConnector = new ServerConnector(server);

                        serverConnector.setHost(bind.getHost());
                        serverConnector.setPort(bind.getPort());

                        return serverConnector;
                    })
                    .collect(Collectors.toList())
                    .toArray(connectors);

                // SET CONNECTORS
                server.setConnectors(connectors);

                // RETURN
                return server;
            });

            // CORS
            config.enableCorsForAllOrigins();

            // CONTEXT PATH
            config.contextPath = HttpConfiguration.getInstance().getBasePath();
            config.wsContextPath = HttpConfiguration.getInstance().getBasePath();
        });
    }

    public API start() {
        javalin.start();
        return this;
    }

    public API stop() {
        javalin.stop();
        return this;
    }

    public int port() {
        return javalin.port();
    }

    public API attribute(Class clazz, Object obj) {
        javalin.attribute(clazz, obj);
        return this;
    }

    public <T> T attribute(Class<T> clazz) {
        return javalin.attribute(clazz);
    }

    public API accessManager(AccessManager access_manager) {
        javalin.config.accessManager(access_manager);
        return this;
    }

    public API addStaticFiles(String class_path) {
        javalin.config.addStaticFiles(class_path);
        return this;
    }

    public API addStaticFiles(String path, Location location) {
        javalin.config.addStaticFiles(path, location);
        return this;
    }

    public <T extends Exception> API exception(Class<T> exceptionClass, ExceptionHandler<? super T> exceptionHandler) {
        javalin.exception(exceptionClass, exceptionHandler);
        return this;
    }

    public API error(int statusCode, Handler handler) {
        javalin.error(statusCode, handler);
        return this;
    }

    public API error(int statusCode, String contentType, Handler handler) {
        javalin.error(statusCode, contentType, handler);
        return this;
    }

    public API get(String path, Handler handler) {
        javalin.get(path, handler);
        return this;
    }

    public API post(String path, Handler handler) {
        javalin.post(path, handler);
        return this;
    }

    public API put(String path, Handler handler) {
        javalin.put(path, handler);
        return this;
    }

    public API patch(String path, Handler handler) {
        javalin.patch(path, handler);
        return this;
    }

    public API delete(String path, Handler handler) {
        javalin.delete(path, handler);
        return this;
    }

    public API head(String path, Handler handler) {
        javalin.head(path, handler);
        return this;
    }

    public API options(String path, Handler handler) {
        javalin.options(path, handler);
        return this;
    }

    public API get(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.get(path, handler, permittedRoles);
        return this;
    }

    public API post(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.post(path, handler, permittedRoles);
        return this;
    }

    public API put(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.put(path, handler, permittedRoles);
        return this;
    }

    public API patch(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.patch(path, handler, permittedRoles);
        return this;
    }

    public API delete(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.delete(path, handler, permittedRoles);
        return this;
    }

    public API head(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.head(path, handler, permittedRoles);
        return this;
    }

    public API options(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.options(path, handler, permittedRoles);
        return this;
    }

    public API sse(String path, Consumer<SseClient> client) {
        javalin.sse(path, client);
        return this;
    }

    public API sse(String path, Consumer<SseClient> client, Set<Role> permittedRoles) {
        javalin.sse(path, client, permittedRoles);
        return this;
    }

    public API before(String path, Handler handler) {
        javalin.before(path, handler);
        return this;
    }

    public API before(Handler handler) {
        javalin.before(handler);
        return this;
    }

    public API after(String path, Handler handler) {
        javalin.after(path, handler);
        return this;
    }

    public API after(Handler handler) {
        javalin.after(handler);
        return this;
    }

    public <T extends Exception> API wsException(Class<T> exceptionClass, WsExceptionHandler<? super T> exceptionHandler) {
        javalin.wsException(exceptionClass, exceptionHandler);
        return this;
    }

    public API ws(String path, Consumer<WsHandler> ws) {
        javalin.ws(path, ws);
        return this;
    }

    public API ws(String path, Consumer<WsHandler> ws, Set<Role> permittedRoles) {
        javalin.ws(path, ws, permittedRoles);
        return this;
    }

    public API wsBefore(String path, Consumer<WsHandler> wsHandler) {
        javalin.wsBefore(path, wsHandler);
        return this;
    }

    public API wsBefore(Consumer<WsHandler> wsHandler) {
        javalin.wsBefore(wsHandler);
        return this;
    }

    public API wsAfter(String path, Consumer<WsHandler> wsHandler) {
        javalin.wsAfter(path, wsHandler);
        return this;
    }

    public API wsAfter(Consumer<WsHandler> wsHandler) {
        javalin.wsAfter(wsHandler);
        return this;
    }

}
