/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.database.connections;

import com.smmx.analysis.dragon.core.metrics.Metrics;
import com.smmx.analysis.dragon.core.metrics.Monitor;
import com.smmx.analysis.dragon.core.metrics.Monitorable;
import com.smmx.analysis.dragon.core.multithreading.MultithreadingConfiguration;
import com.smmx.analysis.dragon.core.pools.Loan;
import com.smmx.analysis.dragon.core.pools.LoanException;
import com.smmx.maria.commons.database.connections.ConnectionShield;
import org.apache.commons.pool2.impl.GenericKeyedObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Osvaldo Miguel Colin
 */
public class ConnectionPool extends GenericKeyedObjectPool<String, ConnectionShield> implements Monitorable {

    // SINGLETON
    private static ConnectionPool INSTANCE;

    static {
        INSTANCE = null;
    }

    public static ConnectionPool getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ConnectionPool();
        }

        return INSTANCE;
    }

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionPool.class);

    // MEMBER
    private ConnectionPool() {
        super(new ConnectionFactory());

        // SET MAX
        this.setMaxTotalPerKey(MultithreadingConfiguration.getInstance().getThreads());
        this.setTimeBetweenEvictionRunsMillis(1000 * 60 * 30); // RUN EVERY 30 MINUTES
        this.setMinEvictableIdleTimeMillis(1000 * 60 * 30); // RUN EVERY 30 MINUTES
        this.setTestWhileIdle(true);

        // REGISTER MONITORABLE
        Monitor.getInstance().register(this);
    }

    public Loan<ConnectionShield> loan(String key) {
        ConnectionShield obj;

        try {
            obj = this.borrowObject(key);
        } catch (Exception ex) {
            throw new LoanException("Failed to borrow connection.", ex);
        }

        return new Loan<ConnectionShield>(obj) {
            @Override
            public void returnObject() {
                ConnectionPool.this.returnObject(key, obj);
            }

            @Override
            public void invalidateObject() {
                try {
                    ConnectionPool.this.invalidateObject(key, obj);
                } catch (Exception ex) {
                    LOGGER.error("Failed to invalidate connection.", ex);
                }
            }
        };
    }

    @Override
    public void collectMetrics(Metrics metrics) {
        metrics.getDictionary("pools")
            .getDictionary("db")
            .put("na", this.getNumActive())
            .put("nc", this.getCreatedCount())
            .put("nd", this.getDestroyedCount());
    }

}
