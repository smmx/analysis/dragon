/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.crontab;

/**
 * @author omiguelc
 */
public enum CrontabState {
    RUNNING, STANDBY, SHUTDOWN
}
