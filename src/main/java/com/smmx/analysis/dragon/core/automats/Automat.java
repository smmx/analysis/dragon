/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.automats;

import java.util.List;

/**
 * @author omiguelc
 */
public abstract class Automat {

    // MEMBER
    private final String name;

    public Automat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    protected abstract void setup();

    protected abstract List<AutomatTask> getTasks();

    protected abstract void cleanUp();

}
