/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.database.builders.privileges;

import java.util.List;

/**
 * @author Osvaldo Miguel Colin
 */
public class ObjectPrivileges {

    private final String objectname;
    private final List<Privilege> privileges;

    public ObjectPrivileges(String objectname, List<Privilege> privileges) {
        this.objectname = objectname;
        this.privileges = privileges;
    }

    public String getObjectName() {
        return objectname;
    }

    public List<Privilege> getPrivileges() {
        return privileges;
    }

}
