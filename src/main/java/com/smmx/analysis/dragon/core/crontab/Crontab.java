/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.core.crontab;

import com.smmx.analysis.dragon.core.metrics.Metrics;
import com.smmx.analysis.dragon.core.metrics.Monitor;
import com.smmx.analysis.dragon.core.metrics.Monitorable;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author omiguelc
 */
public class Crontab implements Monitorable {

    // SINGLETON
    private static Crontab INSTANCE;

    static {
        INSTANCE = null;
    }

    public static Crontab getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Crontab();
        }

        return INSTANCE;
    }

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(Crontab.class);

    // MEMBER
    private final Object mutex;

    private CrontabState state;

    private Crontab() {
        mutex = new Object();

        state = CrontabState.STANDBY;

        // REGISTER MONITORABLE
        Monitor.getInstance().register(this);
    }

    public CrontabState getState() {
        return state;
    }

    public void scheduleJob(JobDetail job_detail, Trigger trigger) {
        try {
            // GET SCHEDULER
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // PUT QUARTS IN STANDBY MODE
            scheduler.scheduleJob(job_detail, trigger);

            // LOG
            LOGGER.debug("Job scheduled: " + job_detail.getDescription() + ".");
        } catch (SchedulerException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    public void start() {
        try {
            LOGGER.info("Starting crontab service.");

            // GET SCHEDULER
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // PUT QUARTS IN STANDBY MODE
            scheduler.start();

            // UPDATE MONITORABLE
            synchronized (mutex) {
                state = CrontabState.RUNNING;
            }

            LOGGER.info("Cronjob service started.");
        } catch (SchedulerException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    public void standby() {
        try {
            LOGGER.info("Cronjob service going to standby.");

            // GET SCHEDULER
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // PUT QUARTS IN STANDBY MODE
            scheduler.standby();

            // UPDATE MONITORABLE
            synchronized (mutex) {
                state = CrontabState.STANDBY;
            }

            LOGGER.info("Cronjob service in standby.");
        } catch (SchedulerException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    public void shutdown() {
        try {
            LOGGER.info("Stopping crontab service.");

            // GET SCHEDULER
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // STOP QUARTZ
            scheduler.shutdown();

            // UPDATE MONITORABLE
            synchronized (mutex) {
                state = CrontabState.SHUTDOWN;
            }

            LOGGER.info("Cronjob service stopped.");
        } catch (SchedulerException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    @Override
    public void collectMetrics(Metrics metrics) {
        synchronized (mutex) {
            metrics.getDictionary("services")
                .getDictionary("crontab")
                .put("state", state.name());
        }
    }
}
