/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon;

import com.smmx.analysis.dragon.core.crontab.Crontab;
import com.smmx.analysis.dragon.core.executors.EventExecutor;
import com.smmx.analysis.dragon.core.executors.ScheduledExecutor;
import com.smmx.analysis.dragon.core.http.API;
import com.smmx.analysis.dragon.modules.APIModule;
import com.smmx.analysis.dragon.modules.AutomationModule;
import com.smmx.analysis.dragon.modules.CoreModule;
import com.smmx.maria.boot.app.ModularApp;

/**
 * @author omiguelc
 */
public class DragonApp extends ModularApp {

    public DragonApp(String name, String description) {
        super(name, description);

        this.addModule(new CoreModule());
        this.addModule(new AutomationModule());
        this.addModule(new APIModule());
    }

    @Override
    public void start() {
        API.getInstance().start();
        Crontab.getInstance().start();
    }

    @Override
    public void stop() {
        API.getInstance().stop();
        Crontab.getInstance().shutdown();
        EventExecutor.getInstance().shutdown();
        ScheduledExecutor.getInstance().shutdown();
    }

}
