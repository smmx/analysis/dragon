/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.monitorables;

import com.smmx.analysis.dragon.core.metrics.Metrics;
import com.smmx.analysis.dragon.core.metrics.Monitorable;

/**
 * @author omiguelc
 */
public class SystemMonitorable implements Monitorable {

    @Override
    public void collectMetrics(Metrics metrics) {
        metrics.getDictionary("system")
            .put("timezone", System.getProperty("user.timezone"))
            .put("unixtime", System.currentTimeMillis() / 1000)
            .put("processors", Runtime.getRuntime().availableProcessors());
    }

}
