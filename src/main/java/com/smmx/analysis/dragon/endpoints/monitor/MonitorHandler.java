/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.dragon.endpoints.monitor;

import com.smmx.analysis.dragon.core.metrics.Monitor;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.json.JSONObject;

/**
 * @author omiguelc
 */
public class MonitorHandler implements Handler {

    @Override
    public void handle(Context ctx) throws Exception {
        String monitor = new JSONObject(
            Monitor.getInstance().getMetrics()
        ).toString(4);

        ctx.result(monitor);
    }
}
